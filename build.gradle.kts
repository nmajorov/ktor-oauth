import org.jetbrains.kotlin.gradle.dsl.Coroutines
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.gradle.jvm.tasks.Jar

val logback_version: String by project
val ktor_version: String by project
val kotlin_version: String by project

plugins {
    application
    kotlin("jvm") version "1.3.70"
    id("org.mikeneck.graalvm-native-image") version "0.5.0"

    id("com.github.johnrengelman.shadow") version "5.0.0"

}

nativeImage {
    setGraalVmHome(System.getProperty("java.home"))
    setMainClass("com.hipster.ApplicationKt")
    setExecutableName("my-ktor")
    setOutputDirectory("$buildDir/executable")
    arguments(
       // "--no-fallback",
       // "--enable-all-security-services",
       // "--enable-url-protocols=http",
       // "-H:ResourceConfigurationFiles=resources/META-INF/native-image/resource-config.json",
       // "-H:ReflectionConfigurationFiles=/Users/nmajorov/workspace/ktor-oauth/resources/META-INF/native-image/reflect-config.json",
        //"--report-unsupported-elements-at-runtime"

   //"-H:Class=com.hipster.ApplicationKt",
    "-H:ConfigurationFileDirectories=./resources/graal-opt",
    "-H:EnableURLProtocols=http",
    "-H:-SpawnIsolates",
    "-H:+JNI" ,
    "--no-server",
    "-H:-UseServiceLoaderFeature" ,
    "--allow-incomplete-classpath",
    "--no-fallback",
    "--initialize-at-run-time=io.netty.channel.kqueue.KQueueEventLoop",
    "--initialize-at-run-time=io.netty.channel.epoll.Native ",
    "--initialize-at-run-time=io.netty.channel.epoll.EpollEventArray",
    "--initialize-at-run-time=io.netty.channel.kqueue.Native ",
    "--initialize-at-run-time=io.netty.channel.unix.Socket",
    "--initialize-at-run-time=io.netty.channel.unix.IovArray",
    "--initialize-at-run-time=io.netty.channel.epoll.EpollEventLoop",
    "--initialize-at-run-time=io.netty.channel.unix.Errors",
    "--initialize-at-run-time=io.netty.channel.unix.Limits",
    "--initialize-at-run-time=io.netty.channel.kqueue.KQueueEventArray",
    "--initialize-at-build-time=ch.qos.logback,org.slf4j"

    )
}



group = "com.hipster"
version = "0.0.1-SNAPSHOT"

application {
    //applicationDefaultJvmArgs = listOf("-agentlib:native-image-agent=config-output-dir=/Users/nmajorov/workspace/ktor-oauth/")
   // mainClassName = "io.ktor.server.netty.EngineMain"

    mainClassName = "io.ktor.server.cio.EngineMain"
}

repositories {
    mavenLocal()
    jcenter()
    maven { url = uri("https://kotlin.bintray.com/ktor") }
}

tasks.withType<KotlinCompile>().configureEach {
    kotlinOptions {
        jvmTarget = "1.8"
        // need use-experimental for Ktor CIO
        freeCompilerArgs += listOf("-Xuse-experimental=kotlin.Experimental", "-progressive")
        // disable -Werror with: ./gradlew -PwarningsAsErrors=false
       // allWarningsAsErrors = project.findProperty("warningsAsErrors") != "false"
    }
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:$kotlin_version")
    //implementation("io.ktor:ktor-server-netty:$ktor_version")
    implementation("io.ktor:ktor-server-cio:$ktor_version")
    implementation("ch.qos.logback:logback-classic:$logback_version")
    implementation("io.ktor:ktor-server-core:$ktor_version")
    implementation("io.ktor:ktor-jackson:$ktor_version")
    testImplementation("io.ktor:ktor-server-tests:$ktor_version")
}

kotlin.sourceSets["main"].kotlin.srcDirs("src")
kotlin.sourceSets["test"].kotlin.srcDirs("test")

sourceSets["main"].resources.srcDirs("resources")
sourceSets["test"].resources.srcDirs("testresources")

tasks.withType<Jar> {
    manifest {
        attributes(
            mapOf(
                "Main-Class" to application.mainClassName
            )
        )
    }
}