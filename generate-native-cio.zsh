#!/usr/bin/env zsh


native-image --report-unsupported-elements-at-runtime \
	--native-image-info \
	-H:ConfigurationFileDirectories=./resources/graal-opt \
	 \
	-H:EnableURLProtocols=http -H:-SpawnIsolates -H:+JNI --no-server \
	-H:ConfigurationFileDirectories=/Users/nmajorov/workspace/ktor-oauth/graal-cio \
	-H:-UseServiceLoaderFeature \
	--allow-incomplete-classpath \
 	--no-fallback \
	--initialize-at-run-time=io.netty.channel.kqueue.KQueueEventLoop \
	--initialize-at-run-time=io.netty.channel.epoll.Native \
	--initialize-at-run-time=io.netty.channel.epoll.EpollEventArray \
	--initialize-at-run-time=io.netty.channel.kqueue.Native \
	--initialize-at-run-time=io.netty.channel.unix.Socket \
       	--initialize-at-run-time=io.netty.channel.unix.IovArray \
	--initialize-at-run-time=io.netty.channel.epoll.EpollEventLoop \
       	--initialize-at-run-time=io.netty.channel.unix.Errors \
	--initialize-at-run-time=io.netty.channel.unix.Limits \
	--initialize-at-run-time=io.netty.channel.kqueue.KQueueEventArray \
	--initialize-at-build-time=ch.qos.logback,org.slf4j \
       	-jar build/libs/ktor-oauth-0.0.1-SNAPSHOT-all.jar ktor-native


#	--initialize-at-build-time=ch.qos.logback.core.status.InfoStatus,org.slf4j.impl.StaticLoggerBinder,org.slf4j.impl.StaticLoggerBinder,ch.qos.logback.classic.Logger,org.slf4j.LoggerFactory,ch.qos.logback.classic.Level,ch.qos.logback.core.spi.AppenderAttachableImpl,ch.qos.logback.classic.PatternLayout \
       
#	--allow-incomplete-classpath \
# -H:ReflectionConfigurationFiles=./resources/graal-opt/reflect-config.json \

